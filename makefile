all:
	latexmk -pdf -pdflatex='pdflatex -interaction=nonstopmode %O %S' VPN_guide_ru
	latexmk -pdf -pdflatex='pdflatex -interaction=nonstopmode %O %S' VPN_guide_en

clean:
	latexmk -C
